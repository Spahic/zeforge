﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using zeforge.Models;

namespace zeforge.Services
{
    public class PresidencyDataStore : IDataStore<Candidate>
    {
        private readonly JsonSerializerSettings _serializerSettings;

        public PresidencyDataStore()
        {
            _serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                NullValueHandling = NullValueHandling.Ignore
            };
            _serializerSettings.Converters.Add(new StringEnumConverter());
        }

        public async Task<bool> AddItemAsync(Candidate item)
        {
            return await Task.FromResult(true);
        }

        public Task<bool> DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<Candidate> GetItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<System.Collections.Generic.IEnumerable<Candidate>> GetItemsAsync(bool forceRefresh = false)
        {
            try
            {
                HttpClient httpClient = new HttpClient();
                var uri = "https://www.izbori.ba/api_2018/race1_memberpresidencycandidatesresult/%22WebResult_2018GEN_2018_10_4_15_40_5%22/701/1";
                HttpResponseMessage response = await httpClient.GetAsync(uri);

                await HandleResponse(response);
                string serialized = await response.Content.ReadAsStringAsync();

                List<Candidate> result = await Task.Run(() =>
                    JsonConvert.DeserializeObject<List<Candidate>>(serialized, _serializerSettings));

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<bool> UpdateItemAsync(Candidate item)
        {
            throw new NotImplementedException();
        }

        private async Task HandleResponse(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();

                if (response.StatusCode == HttpStatusCode.Forbidden ||
                    response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new Exception(content);
                }

                throw new HttpRequestException(content);
            }
        }
    }
}
