﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using zeforge.Models;
using zeforge.Services;

namespace zeforge.ViewModels
{
    public class PredsjednistvoViewModel : BaseViewModel
    {
        public ObservableCollection<Candidate> Items { get; set; }
        public Command LoadItemsCommand { get; set; }
        IDataStore<Candidate> PresidencyDataStore;

        public PredsjednistvoViewModel()
        {
            PresidencyDataStore = DependencyService.Get<IDataStore<Candidate>>();
            Title = "Predsjednistvo";
            Items = new ObservableCollection<Candidate>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
        }

        private async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await PresidencyDataStore.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                //Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
