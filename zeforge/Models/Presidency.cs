﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace zeforge.Models
{
    public class Presidency
    {
        public Presidency()
        {
            Candidates = new List<Candidate>();
        }

        public List<Candidate> Candidates
        {
            get;
            set;
        }
    }
}
