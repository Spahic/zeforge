﻿using System;
using Newtonsoft.Json;

namespace zeforge.Models
{
    public class Candidate
    {
        public int AbsenceAndMobileTeamVotes
        {
            get;
            set;
        }
        public string Code
        {
            get;
            set;
        }
        public int ConfirmedVotes
        {
            get;
            set;
        }
        public bool HaveMandates
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public double Percentage
        {
            get;
            set;
        }
        public int PostOfficeVotes
        {
            get;
            set;
        }
        public int RegularVotes
        {
            get;
            set;
        }
        public int TotalVotes
        {
            get;
            set;
        }
    }


    /*
     * 
     * <Race1_MemberPresidencyCandidatesResult>
<AbsenceAndMobileTeamVotes>917</AbsenceAndMobileTeamVotes>
<Code>00008</Code>
<ConfirmedVotes>105</ConfirmedVotes>
<HaveMandates>false</HaveMandates>
<Name>
BEĆIROVIĆ DENIS - SDP - SOCIJALDEMOKRATSKA PARTIJA BOSNE I HERCEGOVINE
</Name>
<Percentage>33.53</Percentage>
<PostOfficeVotes>2009</PostOfficeVotes>
<RegularVotes>191657</RegularVotes>
<TotalVotes>194688</TotalVotes>
</Race1_MemberPresidencyCandidatesResult>
     * 
     */
}
