﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zeforge.Models
{
    public enum MenuItemType
    {
        Browse,
        About,
        Predsjednistvo
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
