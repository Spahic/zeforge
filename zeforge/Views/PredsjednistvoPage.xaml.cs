﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using zeforge.ViewModels;

namespace zeforge.Views
{
    public partial class PredsjednistvoPage : ContentPage
    {
        PredsjednistvoViewModel viewModel;

        public PredsjednistvoPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new PredsjednistvoViewModel();
        }

        public void Add_Clicked(object sender, EventArgs e)
        {
            DisplayAlert("Test", "testttttttt", "Odustani");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }
    }
}
